<?php

class pagarServicio
{
    /**
     * @var string nombre convenio {nillable=1, minOccurs=0}
     * @soap
     */
    public $nombreConvenio;
    /**
     * @var string numero factura {nillable=1, minOccurs=0}
     * @soap
     */
    public $numeroFactura;
    /**
     * @var string valor {nillable=1, minOccurs=0}
     * @soap
     */
    public $valor;
}