<?php
use mikehaertl\pdftk\Pdf;

class WebserviceController extends CController
{


	public function actions()
	{
		return array(
            'index'=>array(
                'class'=>'CWebServiceAction',
                'serviceOptions' => array(
                    'soapVersion'=>'1.2',
                ),
            ),
		);
	}

    /**
     * @param crearConvenio the symbol of the stock
     * @return string the stock price
     * @soap
     */
    public function crearConvenio($symbol)
    {
        return isset($symbol);
    }

    /**
     * @param eliminarConvenio the symbol of the stock
     * @return string the stock price
     * @soap
     */
    public function eliminarConvenio($symbol)
    {
        return isset($symbol);
    }

    /**
     * @param validarUsuario the symbol of the stock
     * @return string the stock price
     * @soap
     */
    public function validarUsuario($symbol)
    {
        return isset($symbol);
    }

    /**
     * @param consultarSaldoCuenta the symbol of the stock
     * @return string the stock price
     * @soap
     */
    public function consultarSaldoCuenta($symbol)
    {
        return isset($symbol);
    }

    /**
     * @param consultarValorPagar the symbol of the stock
     * @return string the stock price
     * @soap
     */
    public function consultarValorPagar($symbol)
    {
        return isset($symbol);
    }

    /**
     * @param pagarServicio the symbol of the stock
     * @return string the stock price
     * @soap
     */
    public function pagarServicio($symbol)
    {
        return isset($symbol);
    }

    /**
     * @param compensacion the symbol of the stock
     * @return string the stock price
     * @soap
     */
    public function compensacion($symbol)
    {
        return isset($symbol);
    }

    /**
     * @param recibirIncidente the symbol of the stock
     * @return string the stock price
     * @soap
     */
    public function recibirIncidente($symbol)
    {
        return isset($symbol);
    }

}
