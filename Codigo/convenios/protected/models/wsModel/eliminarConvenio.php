<?php

class eliminarConvenio
{
    /**
     * @var string nombre convenio {nillable=1, minOccurs=0}
     * @soap
     */
    public $nombreConvenio;
    /**
     * @var string motivo {nillable=1, minOccurs=0}
     * @soap
     */
    public $motivo;
}