<?php

class compensacion
{
    /**
     * @var string nombre convenio {nillable=1, minOccurs=0}
     * @soap
     */
    public $nombreConvenio;
    /**
     * @var string numero factura {nillable=1, minOccurs=0}
     * @soap
     */
    public $numeroFactura;
}