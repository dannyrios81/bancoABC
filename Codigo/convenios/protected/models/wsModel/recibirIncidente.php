<?php

class recibirIncidente
{
    /**
     * @var string nombre convenio {nillable=1, minOccurs=0}
     * @soap
     */
    public $nombreConvenio;
    /**
     * @var string numero factura {nillable=1, minOccurs=0}
     * @soap
     */
    public $numeroFactura;
    /**
     * @var string valor factura {nillable=1, minOccurs=0}
     * @soap
     */
    public $valorFactura;
    /**
     * @var string motivo {nillable=1, minOccurs=0}
     * @soap
     */
    public $motivo;
}