<?php

class consultarSaldoCuenta
{
    /**
     * @var string nombre usuario {nillable=1, minOccurs=0}
     * @soap
     */
    public $nombreUsuario;
    /**
     * @var string numero cuenta {nillable=1, minOccurs=0}
     * @soap
     */
    public $numeroCuenta;
    /**
     * @var string numero identificacion {nillable=1, minOccurs=0}
     * @soap
     */
    public $valor;
}