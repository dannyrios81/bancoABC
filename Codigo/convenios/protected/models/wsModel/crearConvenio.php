<?php

class crearConvenio
{
    /**
     * @var string nombre convenio {nillable=1, minOccurs=0}
     * @soap
     */
    public $nombreConvenio;
    /**
     * @var string tipo convenio {nillable=1, minOccurs=0}
     * @soap
     */
    public $tipoConvenio;

}