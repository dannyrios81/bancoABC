<?php

class foo
{
    /**
     * @var string nombre
     * @soap
     */
    public $nombre;
    /**
     * @var string apellido
     * @soap
     */
    public $apellido;
    /**
     * @var string cedula
     * @soap
     */
    public $cedula;
    /**
     * @var string apellido2
     * @soap
     */
    public $apellido2;

}